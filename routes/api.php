<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/* ruta para cuanta usuario con saldo */
Route::get('CuentaUsuario', 'App\Http\Controllers\ControllerCuentaUsuario@getUsuarioSaldo');
/* ruta para para crear usuario */
Route::post('Nuwcuenta', 'App\Http\Controllers\ControllerCuentaUsuario@CrearCuenta');
/* ruta para tipo cuenta */
Route::get('tipoCuenta', 'App\Http\Controllers\ControllerCuentaUsuario@getTipocuenta');

/* ruta para actulizacion saldo usuario */
Route::post('ActulizarC', 'App\Http\Controllers\ControllerCuentaUsuario@consignarc');

/* ruta para pasar parametro soldo por id */
Route::post('Findsaldo', 'App\Http\Controllers\ControllerCuentaUsuario@findSado');

/* ruta para retirar cuenta */
Route::post('RetiroC', 'App\Http\Controllers\ControllerCuentaUsuario@retirar');


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
