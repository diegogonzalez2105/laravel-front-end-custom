<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class usuario_cuenta extends Model
{
    protected $table = "usuario_cuenta";
    protected $primaryKey = "id";
    protected $fillable = [
        'Fk_users','Fk_tipo_cuenta ','Mumero','created_at', 'updated_at','delete_at'
    ];
    protected $hidden = array('created_at', 'updated_at','delete_at');
    use HasFactory;
}
