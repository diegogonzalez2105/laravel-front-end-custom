<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class saldo_cuenta extends Model
{
    protected $table = "saldo_cuenta";
    protected $primaryKey = "id";
    protected $fillable = [
        'Saldo','Fk_usuario_cuenta'
    ];
    protected $hidden = array('created_at','updated_at','delete_at');



    public function Saldofind($id) {
        return DB::table('saldo_cuenta as t1')
                       ->where('t1.id','=',$id)
                        ->select('t1.id','t1.Saldo')
                        ->get();
    }






    use HasFactory;
}
