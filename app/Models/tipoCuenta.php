<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tipoCuenta extends Model
{
    protected $table = "tipo_cuenta";
    protected $primaryKey = "id";
    protected $fillable = [
        'Tipo','created_at', 'updated_at','delete_at'
    ];

    public function TipoCuenta() {
        return DB::table('tipo_cuenta as t1')
                        ->select('t1.id','t1.Tipo')
                        ->get();
    }
    use HasFactory;
}
