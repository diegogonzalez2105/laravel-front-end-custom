<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CuentaUsuario extends Model
{
     protected $table = "users";
    protected $primaryKey = "id";
    protected $fillable = [
        'id','name','created_at', 'updated_at'
    ];
    protected $hidden = array('created_at', 'updated_at');


    public function CuentaUsuario() {
        return DB::table('users as t1')
                       ->join('usuario_cuenta as t2', 't1.id', '=', 't2.Fk_users')
                       ->join('saldo_cuenta as t3', 't2.Fk_users', '=', 't3.Fk_usuario_cuenta')
                        ->select('t1.id','t1.name', 't2.Mumero', 't3.Saldo','t3.id as idsaldo')
                        ->get();
    }


    public function Usuariofind($id) {
        return DB::table('users as t1')
                       ->where('t1.id','=',$id)
                        ->select('t1.id')
                        ->get();
    }
    use HasFactory;

}
