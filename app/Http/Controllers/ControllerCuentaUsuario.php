<?php

namespace App\Http\Controllers;
use Response;
use App\Models\tipoCuenta;
use App\Models\CuentaUsuario;
use App\Models\saldo_cuenta;
use App\Models\usuario_cuenta;
use Illuminate\Http\Request;

class ControllerCuentaUsuario extends Controller
{
    /* Lista todo los usuarios con soldo y numero de cuenta */
    public function getUsuarioSaldo(Request $request)
    {

            $json = [];
            $Usuarios = new CuentaUsuario();
            $resul = $Usuarios->CuentaUsuario();
            for ($ew = 0; $ew < count($resul); $ew++) {
                array_push($json, $resul[$ew]);
            }
            if (count($json) > 0) {
                return Response::json(
                    array(
                        'data' => $json,
                        'status' => '1',
                        'msg' => 'Existen datos.'
                    ),
                    200
                );
            } else {
                return Response::json(
                    array(
                        'status' => '0',
                        'msg' => 'No existen datos.'
                    ),
                    200
                );
            }

    }
/* Lista tipo de cuenta  */
    public function getTipocuenta(Request $request)
    {

            $json = [];
            $ObjetoTipo = new tipoCuenta();
            $resul = $ObjetoTipo->TipoCuenta();
            for ($ew = 0; $ew < count($resul); $ew++) {
                array_push($json, $resul[$ew]);
            }
            if (count($json) > 0) {
                return Response::json(
                    array(
                        'data' => $json,
                        'status' => '1',
                        'msg' => 'Existen datos tipo cuenta.'
                    ),
                    200
                );
            } else {
                return Response::json(
                    array(
                        'status' => '0',
                        'msg' => 'No existen datos tipo cuenta.'
                    ),
                    200
                );
            }

    }


    public function findSado(Request $request)
    {

            $json = [];
            $ObjetoTipo = new saldo_cuenta();
            $resul = $ObjetoTipo->Saldofind($request->id);
            for ($ew = 0; $ew < count($resul); $ew++) {
                array_push($json, $resul[$ew]);
            }
            if (count($json) > 0) {
                return Response::json(
                    array(
                        'data' => $json,
                        'status' => '1',
                        'msg' => 'Existen datos saldo.'
                    ),
                    200
                );
            } else {
                return Response::json(
                    array(
                        'status' => '0',
                        'msg' => 'No existen saldo.'
                    ),
                    200
                );
            }

    }

/* Crea nueba cuenta */
    public function CrearCuenta(Request $request)
    {

                    $newCuenta = new CuentaUsuario();
                    $newCuenta->name = trim($request->nombreU);
                    /* var_dump($newCuenta); */
                    if ($newCuenta->save()) {
                        $newSal = new saldo_cuenta();
                        $newSal->Saldo =$request->Newsaldo;
                        $newSal->Fk_usuario_cuenta  = $newCuenta->id;
                        $newSal->save();
                        $newUsuario = new usuario_cuenta();
                        $newUsuario->Fk_users =$newCuenta->id;
                        $newUsuario->Fk_tipo_cuenta = $request->tipo;
                        $newUsuario->Mumero = uniqid() . date('Ymdhsi');
                        $newUsuario->save();
                        return Response::json(
                            array(
                                'status' => '1',
                                'msg' => 'se agrego numero de cuenta',
                                'data'=>$newCuenta->id
                            ), 200);


                }else{
                    return Response::json(
                        array(
                            'status' => '0',
                            'msg' => 'No se agrego usuario'
                        ), 200);
                }

    }

/* Modificacion del saldo cuenta */
    public function consignarc(Request $request){
                $Updatesaldo = saldo_cuenta::find($request->id);
                $newSal = new saldo_cuenta();
                $resul = $newSal->Saldofind($request->id);
                $suma = $resul[0]->Saldo+$request->NewsaldoC;
                $Updatesaldo->Saldo =$suma;
                if ($Updatesaldo->save()) {
                    return Response::json(
                                    array(
                                        'status' => '1',
                                        'msg' => 'la consignacion fue realizada con exito.'
                                    ), 200);
                } else {
                    return Response::json(
                                    array(
                                        'status' => '0',
                                        'msg' => 'No se logro realizar consignacion.'
                                    ), 200);
                }

    }

/* funcion para restar y si el parametro que recibe es mayor al saldo no deja devuelve validacion */

    public function retirar(Request $request){
        $Updatesaldo = saldo_cuenta::find($request->id);
        $newSal = new saldo_cuenta();
        $resul = $newSal->Saldofind($request->id);
        $resta = $resul[0]->Saldo-$request->NewsaldoC;
        if ($request->NewsaldoC>$resul[0]->Saldo) {
            return Response::json(
                array(
                    'status' => '2',
                    'msg' => 'No tiene saldo suficiente.'
                ), 200);
        }
        $Updatesaldo->Saldo =$resta;
        if ($Updatesaldo->save()) {
            return Response::json(
                            array(
                                'status' => '1',
                                'msg' => 'Retiro exitoso.'
                            ), 200);
        } else {
            return Response::json(
                            array(
                                'status' => '0',
                                'msg' => 'No se logro realizar Retiro.'
                            ), 200);
        }

}




}
